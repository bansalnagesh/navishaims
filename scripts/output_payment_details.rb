#Script to enter the orders in the mongo system
#Usage is ruby output_status.rb portal csv_filename
#The script can be ran from anywhere
#The input file needs to be unprocessed and as downloaded from the portals directly
#Currently only processing Flipkart and Snapdeal.
#TBD: amazon and cash order processing

require 'mongo'
require 'json'
require 'csv'

if ARGV[0].nil?
	abort('Usage ruby output_payment_details.rb portal')
end

@portal = ARGV[0].downcase

DB = Mongo::Connection.new('localhost',27017,:slave_ok => true, :pool_size => 20, :pool_timeout => 5).db('navisha')

@transactions_db = DB['transactions']
@orders_db = DB['orders']

@csv_paid_file = "/Users/bansalnagesh/Work/RubymineProjects/Navisha/reports/#{@portal}_paid_orders.csv"
@csv_unpaid_file = "/Users/bansalnagesh/Work/RubymineProjects/Navisha/reports/#{@portal}_unpaid_orders.csv"

CSV.open(@csv_paid_file,'a') do |writer|
	writer<<['order_code','order_date','invoice_number','sku','quantity','net_payment','gross_profit','payout_details']
	@transactions_db.find({portal: @portal,payout_details:{'$exists'=>true}}).each do |payout_details|
		writer << [payout_details['order_code'], payout_details['order_date'], payout_details['invoice_number'],payout_details['sku'],payout_details['quantity'],payout_details['net_payment'],payout_details['gross_profit'],payout_details['payout_details']]
	end
end

CSV.open(@csv_unpaid_file,'a') do |writer|
	writer<<['order_code','order_date','invoice_number','invoice_date','sku','quantity','status']
	@transactions_db.find({portal: @portal,payout_details:{'$exists'=>false}}).each do |payout_details|
		status = @orders_db.find_one({order_code:payout_details['order_code']},{fields:{status:1}})
		writer << [payout_details['order_code'], payout_details['order_date'], payout_details['invoice_number'],payout_details['invoice_date'],payout_details['sku'],payout_details['quantity'],status['status']]
	end
end

