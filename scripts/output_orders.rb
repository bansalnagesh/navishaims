#Script to enter the orders in the mongo system
#Usage is ruby output_status.rb portal csv_filename
#The script can be ran from anywhere
#The input file needs to be unprocessed and as downloaded from the portals directly
#Currently only processing Flipkart and Snapdeal.
#TBD: amazon and cash order processing

require 'mongo'
require 'json'
require 'csv'

if ARGV[0].nil?
	abort('Usage ruby output_status.rb portal')
end

@portal = ARGV[0].downcase

DB = Mongo::Connection.new('localhost',27017,:slave_ok => true, :pool_size => 20, :pool_timeout => 5).db('navisha')

@orders_db = DB['orders']

@csv_file = "/Users/bansalnagesh/Work/RubymineProjects/Navisha/reports/#{@portal}_orders.csv"

CSV.open(@csv_file,'a') do |writer|
	writer<<['order_code','order_date','invoice_number','invoice_date','sku','quantity','status','payment_status']
	@orders_db.find({portal: @portal}).sort({order_code:1}).each do |orders|
		writer << [orders['order_code'], orders['order_date'], orders['invoice_number'],orders['invoice_date'],orders['sku'],orders['quantity'],orders['status'],orders['payment_status']]
	end
end
