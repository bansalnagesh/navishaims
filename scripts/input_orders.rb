#Script to enter the orders in the mongo system
#Usage is ruby input_orders.rb portal csv_filename
#The script can be ran from anywhere
#The input file needs to be unprocessed and as downloaded from the portals directly
#Currently only processing Flipkart and Snapdeal.
#TBD: amazon and cash order processing

require 'mongo'
require 'json'
require 'csv'

if ARGV[0].nil? || ARGV[1].nil?
	abort('Usage ruby input_orders.rb portal csv_filename')
end

@portal = ARGV[0].downcase
@csv_file = ARGV[1]

@log_path = "/Users/bansalnagesh/Work/RubymineProjects/Navisha/log/input_orders.log"
@sku_log_path = '/Users/bansalnagesh/Work/RubymineProjects/Navisha/log/sku_to_add.log'

begin
	@logs = File.open(@log_path,'a')
	@sku_logs = File.open(@sku_log_path,'a')
rescue
	puts "log paths are #{@log_path} and #{@sku_log_path}"
	abort("Error: Couldn't open log files")
end

@FK_SKU_CORRECTION = {
	'crystalnipc3l' => 'crystalnipc3ltall',
	'loganipc8l' => 'loganipcniil8lwide',
	'libertypcniil2l' => 'libertypcibil2l'
}

DB = Mongo::Connection.new('localhost',27017,:slave_ok => true, :pool_size => 20, :pool_timeout => 5).db('navisha')

def log_message(kvp_hash,log_file = @logs)
	kvp_str = kvp_hash.map{|k,v| "#{k} = #{v.to_json}"}.join(' ')
	log_message = "#{Time.now} #{kvp_str} \n"
	log_file.puts(log_message)
	log_file.flush
end

@orders_db = DB['orders']
@transactions_db = DB['transactions']
@consolidated_db = DB['consolidated']


def update_stock_count(sku,quantity,order_code)
    existing_data = @consolidated_db.find_one({sku: sku})
    if existing_data
        if parent_sku = existing_data['parent_sku']
				parent_sku.each do |psku|
                @consolidated_db.update({sku:psku}, {'$inc'=> {inventory:-quantity}})
				nested_data = @consolidated_db.find_one({sku: psku})
				update_stock_count(psku,quantity,order_code) if nested_data['parent_sku']
            end
        end
        @consolidated_db.update({sku:sku},{'$inc' => {inventory:-quantity}})
	else
		kvp_hash = {code:'update_stock_count/sku/not_found', sku:sku, csv_file:@csv_file, order_code:order_code}
        log_message(kvp_hash,@sku_logs)
		# raise "SKU could not be found #{sku}"
    end

end

def flipkart_order_entry
	@portal = 'flipkart'
	csv_contents = CSV.read(@csv_file)
	csv_contents.shift
	csv_contents.each do |row|
		order_code = row[4]
		existing_order = @orders_db.find_one({order_code: order_code})
		order_date =   row[0]
		invoice_number = row[16]
		invoice_date = row[18]
		sku = row[8].downcase
		#SKU Correction
		if ['crystalnipc3l','loganipc8l','libertypcniil2l'].include? sku
			sku = @FK_SKU_CORRECTION[sku]
		end
		status = row[9]
		quantity = row[14].to_i
		ship_details = {
			provider: 'flipkart',
			awb: row[33],
			date: row[29],
			city: row[24]
		}
		if !existing_order
			update_stock_count(sku,quantity,order_code)
			order_entry = {
				order_code:order_code,
				order_date:  order_date,
				portal:@portal,
				invoice_number: invoice_number,
				invoice_date: invoice_date,
				sku: sku,
				quantity: quantity,
				ship_details: ship_details,
				status: status
			}
			@orders_db.insert(order_entry)
			transaction_entry = {
				order_code:order_code,
				order_date:  order_date,
				portal:@portal,
				invoice_number: invoice_number,
				invoice_date: invoice_date,
				sku: sku,
				quantity: quantity,
				selling_price:row[10].to_f,
				shipping_charged:row[11].to_f,
				total_amount: row[15].to_f,
				invoice_amount:row[17].to_f,
				vat_charged:row[19].to_f,
			}
			product = @consolidated_db.find_one({sku:sku})
			transaction_entry[:actual_vat] = row[15].to_f - (row[15].to_f / (1 + (product['tax'].to_f)/100) )
			@transactions_db.insert(transaction_entry)
		else
			puts "Existing in the database #{existing_order.inspect}"
			puts "From the CSV #{row}"
			if existing_order['status'] != status
				@orders_db.update({order_code:order_code},{status:status,ship_details:ship_details})
			end
		end
	end
end

def snapdeal_order_entry
	@portal = 'snapdeal'
	csv_contents = CSV.read(@csv_file,"r:windows-1250")
	csv_contents.shift
	csv_contents.each do |row|
		order_code = row[2]
		existing_order = @orders_db.find_one({order_code: order_code})
		order_date =   row[4]
		invoice_number = row[10]
		invoice_date = row[11]
		sku = row[19].downcase
		status = row[13]
		quantity = 1
		ship_details = {
			provider: row[7],
			awb: row[6],
			date: row[14],
			city: row[8],
            delivered_on: row[16]
		}
		if !existing_order
			update_stock_count(sku,quantity,order_code)
			order_entry = {
				order_code:order_code,
				order_date:  order_date,
				portal:@portal,
				invoice_number: invoice_number,
				invoice_date: invoice_date,
				sku: sku,
				quantity: quantity,
				ship_details: ship_details,
				status: status
			}
			@orders_db.insert(order_entry)
			transaction_entry = {
				order_code:order_code,
				order_date:  order_date,
				portal:@portal,
				invoice_number: invoice_number,
				invoice_date: invoice_date,
				sku: sku,
				quantity: quantity,
				# selling_price:row[10],
				# shipping_charged:row[11],
				# total_amount: row[15],
				# invoice_amount:row[17],
				# vat_charged:row[19],
			}
			# product = @consolidated_db.find_one({sku:sku},{tax:1})
			# transaction_entry[:actual_vat] = row[15] - (row[15].to_f / (1 + (product['tax'].to_f)/100) )
			@transactions_db.insert(transaction_entry)
        else
			puts "Existing in the database #{existing_order}"
			puts "From the CSV #{row}"
			if existing_order['status'] != status
				@orders_db.update({order_code:order_code},{status:status,ship_details:ship_details})
			end
		end
	end
end

case @portal
	when 'flipkart'
		flipkart_order_entry
	when 'snapdeal'
		snapdeal_order_entry
	when 'amazon'
		amazon_order_entry
	when 'paytm'
		paytm_order_entry
	when 'cash'
		cash_order_entry
	else
		puts "Where the hell did you sell?"
end

# def amazon_order_entry
#     @portal = 'amazon'
#     csv_contents = CSV.read(@csv_file)
#     csv_contents.shift
#     csv_contents.each do |row|
#         order_code = row[2]
#         existing_order = @orders_db.find_one({order_code: order_code})
#         order_date =   row[4]
#         invoice_number = row[10]
#         invoice_date = row[11]
#         sku = row[19]
#         status = row[13]
#         quantity = 1
#         ship_details = {
#             provider: row[7],
#             awb: row[6],
#             date: row[14],
#             city: row[8],
#             delivered_on: row[16]
#         }
#         if !existing_order
#             order_entry = {
#                 order_code:order_code,
#                 order_date:  order_date,
#                 portal:@portal,
#                 invoice_number: invoice_number,
#                 invoice_date: invoice_date,
#                 sku: sku,
#                 quantity: quantity,
#                 ship_details: ship_details,
#                 status: status
#             }
#             @orders_db.insert(order_entry)
#             transaction_entry = {
#                 order_code:order_code,
#                 order_date:  order_date,
#                 portal:@portal,
#                 invoice_number: invoice_number,
#                 invoice_date: invoice_date,
#                 sku: sku,
#                 quantity: quantity,
#                 # selling_price:row[10],
#                 # shipping_charged:row[11],
#                 # total_amount: row[15],
#                 # invoice_amount:row[17],
#                 # vat_charged:row[19],
#             }
#             # product = @input_db.find_one({sku:sku},{tax:1})
#             # transaction_entry[:actual_vat] = row[15] - (row[15].to_f / (1 + (product['tax'].to_f)/100) )
#             @transactions_db.insert(transaction_entry)
#         else
#             puts "Existing in the database #{existing_order}"
#             puts "From the CSV #{row}"
#             if existing_order['status'] != status
#                 @orders_db.update({order_code:order_code},{status:status,ship_details:ship_details})
#             end
#         end
#     end
# end






