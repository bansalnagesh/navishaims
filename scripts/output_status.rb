#Script to enter the orders in the mongo system
#Usage is ruby output_status.rb portal csv_filename
#The script can be ran from anywhere
#The input file needs to be unprocessed and as downloaded from the portals directly
#Currently only processing Flipkart and Snapdeal.
#TBD: amazon and cash order processing

require 'mongo'
require 'json'
require 'csv'

if ARGV[0].nil?
	abort('Usage ruby output_status.rb portal')
end

@portal = ARGV[0].downcase

DB = Mongo::Connection.new('localhost',27017,:slave_ok => true, :pool_size => 20, :pool_timeout => 5).db('navisha')

@consolidated_db = DB['consolidated']

@csv_file = "/Users/bansalnagesh/Work/RubymineProjects/Navisha/reports/#{@portal}_status.csv"

CSV.open(@csv_file,'a') do |writer|
	writer<<['sku','price_without_tax','tax %', 'total_CP','MRP on portal', 'SP', 'shipping_charges']
	@consolidated_db.find({"#{@portal}_status"=>{'$exists'=>true}}).each do |flipkart_products|
		writer<<[flipkart_products['sku'],flipkart_products['price_without_tax'],flipkart_products['tax'],flipkart_products['cost_price'],flipkart_products["#{@portal}_status"]['mrp'],flipkart_products["#{@portal}_status"]['selling_price'],flipkart_products["#{@portal}_status"]['shipping_charges']]
	end
	@consolidated_db.find({"#{@portal}_status"=>{'$exists'=>false}}).each do |flipkart_products|
		writer<<[flipkart_products['sku'],flipkart_products['price_without_tax'],flipkart_products['tax'],flipkart_products['cost_price']]
	end
end
