#Script to enter the bills in the mongo system
#Usage is ruby input_inventory.rb bill_id csv_filename
#The script can be ran from anywhere
#The input file needs to be of the following format SKU, Price, Tax %, Quantity
# run


require 'mongo'
require 'json'
require 'csv'

if ARGV[0].nil? || ARGV[1].nil?
  abort('Usage ruby input_inventory.rb bill_id csv_filename')
end

@bill_id = ARGV[0].downcase
@csv_file = ARGV[1]

DB = Mongo::Connection.new('localhost',27017,:slave_ok => true, :pool_size => 20, :pool_timeout => 5).db('navisha')

@input_db = DB['product_in']
@consolidated_db = DB['consolidated']

csv_contents = CSV.read(@csv_file)
csv_contents.shift
csv_contents.each do |row|
  sku = row[0].downcase
  price = row[1].to_f
  tax = row[2].to_f
  quantity = row[3].to_i
  parentSku = row[4].split(",").map{|v| v.downcase} rescue nil
  childrenSku = row[5].split(",").map{|v| v.downcase} rescue nil
  net_price = price*(1+(tax/100.0))
  bill_entries = {
      sku: sku,
      bill_id: @bill_id,
      price: price,
      tax: tax,
      net_price: net_price,
      quantity: quantity
  }
  @input_db.insert(bill_entries) if parentSku.nil?
  existing_data = @consolidated_db.find_one({sku: sku})
  if existing_data.nil?
    consolidate_entry = {
        sku: sku,
        price_without_tax: price,
        inventory: quantity,
        cost_price: net_price,
        tax: tax,
        mrp: 0
    }
    consolidate_entry[:parentsku] = parentSku unless parentSku.nil?
    consolidate_entry[:childrensku] = childrenSku unless childrenSku.nil?
    @consolidated_db.insert(consolidate_entry)
  else
    old_inventory = existing_data['inventory'].to_i
    old_price_wo_tax = existing_data['price_without_tax'].to_f
    old_cost = existing_data['cost_price'].to_f
    old_parentsku = existing_data['parentsku']  rescue nil
    old_childrensku = existing_data['childrensku'] rescue nil
    parentSku = parentSku.nil? ? old_parentsku.nil? ? nil : old_parentsku : old_parentsku.nil? ? parentSku : parentSku + old_parentsku
    childrenSku = childrenSku.nil? ? old_childrensku.nil? ? nil : old_childrensku : old_childrensku.nil? ? childrenSku : childrenSku  + old_childrensku
    inventory =  old_inventory + quantity
    price_wo_tax = price != old_price_wo_tax ? ((old_inventory * old_price_wo_tax + quantity * price) / (old_inventory+quantity)) : old_price_wo_tax
    cost_price = cost_price != old_cost ? ((old_inventory * old_cost + quantity * net_price) / (old_inventory+quantity)) : old_cost
    fields_to_set = {inventory:inventory,
                     price_without_tax: price_wo_tax,
                     cost_price:cost_price}
    fields_to_set[:parentsku] = parentSku unless parentSku.nil?
    fields_to_set[:childrensku] = childrenSku unless childrenSku.nil?
    @consolidated_db.update({sku:sku},{'$set' =>fields_to_set })
  end
end