#Script to enter the orders in the mongo system
#Usage is ruby input_orders.rb portal csv_filename
#The script can be ran from anywhere
#The input file needs to be unprocessed and as downloaded from the portals directly
#Currently only processing Flipkart and Snapdeal.
#TBD: amazon and cash order processing

require 'mongo'
require 'json'
require 'csv'

if ARGV[0].nil? || ARGV[1].nil?
	abort('Usage ruby input_orders.rb portal csv_filename')
end

@portal = ARGV[0].downcase
@csv_file = ARGV[1]

@log_path = "/Users/bansalnagesh/Work/RubymineProjects/Navisha/log/input_transaction.log"
@sku_log_path = '/Users/bansalnagesh/Work/RubymineProjects/Navisha/log/sku_to_add.log'

begin
	@logs = File.open(@log_path,'a')
	@sku_logs = File.open(@sku_log_path,'a')
rescue
	puts "log paths are #{@log_path} and #{@sku_log_path}"
	abort("Error: Couldn't open log files")
end

DB = Mongo::Connection.new('localhost',27017,:slave_ok => true, :pool_size => 20, :pool_timeout => 5).db('navisha')

def log_message(kvp_hash,log_file = @logs)
	kvp_str = kvp_hash.map{|k,v| "#{k} = #{v.to_json}"}.join(' ')
	log_message = "#{Time.now} #{kvp_str} \n"
	log_file.puts(log_message)
	log_file.flush
end

@FK_SKU_CORRECTION = {
	'crystalnipc3l' => 'crystalnipc3ltall',
	'loganipc8l' => 'loganipcniil8lwide',
	'libertypcniil2l' => 'libertypcibil2l'
}

@orders_db = DB['orders']
@transactions_db = DB['transactions']
@consolidated_db = DB['consolidated']

def flipkart_transaction_entry
	@portal = 'flipkart'
	csv_contents = CSV.read(@csv_file)
	csv_contents.shift
	csv_contents.each do |row|
		order_code = row[5]
		sku = row[3].downcase
		#SKU Correction
		if ['crystalnipc3l','loganipc8l','libertypcniil2l'].include? sku
			sku = @FK_SKU_CORRECTION[sku]
		end

		existing_order = @orders_db.find_one({order_code: order_code})
		product = @consolidated_db.find_one({sku:sku})
		# tax_paid = product['cost_price'] - product['price_without_tax']
		if !existing_order
			kvp_hash = {code:'transaction/order/not_found', csv_file:@csv_file, sku: sku, order_code:order_code}
			log_message(kvp_hash,@logs)
		else
			@orders_db.update({order_code: order_code},{'$set' => {payment_status:'settled'}})
			sale_price = row[14].to_f
			final_amount = row[20].to_f
			gross_profit = final_amount > 0 ? final_amount - product['cost_price'] : final_amount
			payout_details = {
				selling_price: sale_price,
				merchant_cut: final_amount,
				marketing_fee: row[22].to_f.abs,
				collection_charge: row[25].to_f.abs,
				courier_charge: row[28].to_f.abs,
				service_tax: row[19].to_f.abs,
				total_commission: row[18].to_f.abs+row[19].to_f.abs,
				final_amount: final_amount
			}
			fields = {
				'$push' => {payout_details: payout_details}
			}
			@transactions_db.update({order_code:order_code}, fields)
			@transactions_db.update({order_code:order_code}, {'$inc'=> {net_payment:final_amount,gross_profit:gross_profit}})
		end
	end
end

def snapdeal_transaction_entry
	@portal = 'snapdeal'
	csv_contents = CSV.read(@csv_file,"r:windows-1250")
	csv_contents.shift
	csv_contents.each do |row|
		order_code = row[0]
		sku = row[27].downcase
		existing_order = @orders_db.find_one({order_code: order_code})
		product = @consolidated_db.find_one({sku:sku})
		if !existing_order
			kvp_hash = {code:'transaction/order/not_found', csv_file:@csv_file, sku: sku, order_code:order_code}
			log_message(kvp_hash,@logs)
		else
			@orders_db.update({order_code: order_code},{'$set' => {payment_status:'settled'}})
			sale_price = row[7].to_f
			final_amount = row[34].to_f
			gross_profit = final_amount.abs - product['cost_price']
			fields_to_set = {}
			if sale_price > 0
				fields_to_set[:selling_price] = sale_price
				fields_to_set[:shipping_charged] = 0
				fields_to_set[:total_amount] = sale_price
				fields_to_set[:invoice_amount] = sale_price
				fields_to_set[:actual_vat] = sale_price - sale_price/(1 + (product['tax'].to_f)/100)
			end
			payout_details = {
				selling_price: sale_price,
				merchant_cut: row[15].to_f,
				fulfillment_fee: row[17].to_f,
				fulfillment_fee_waiver: row[19].to_f,
				marketing_fee: row[21].to_f,
				collection_charge: row[24].to_f,
				courier_charge: row[25].to_f,
				total_commission: row[26].to_f,
				final_amount: final_amount
			}
			fields = {
				'$push' => {payout_details: payout_details}
			}
			fields['$set'] = fields_to_set if (!fields_to_set.nil? && !fields_to_set.empty?)
			@transactions_db.update({order_code:order_code}, fields)
			gross_profit = sale_price > 0 ? gross_profit : -gross_profit
			@transactions_db.update({order_code:order_code}, {'$inc'=> {net_payment:final_amount,gross_profit:gross_profit}})
		end
	end
end

case @portal
	when 'flipkart'
		flipkart_transaction_entry
	when 'snapdeal'
		snapdeal_transaction_entry
	when 'amazon'
		amazon_order_entry
	when 'paytm'
		paytm_order_entry
	when 'cash'
		cash_order_entry
	else
		puts "Where the hell did you sell?"
end

# def amazon_order_entry
#     @portal = 'amazon'
#     csv_contents = CSV.read(@csv_file)
#     csv_contents.shift
#     csv_contents.each do |row|
#         order_code = row[2]
#         existing_order = @orders_db.find_one({order_code: order_code})
#         order_date =   row[4]
#         invoice_number = row[10]
#         invoice_date = row[11]
#         sku = row[19]
#         status = row[13]
#         quantity = 1
#         ship_details = {
#             provider: row[7],
#             awb: row[6],
#             date: row[14],
#             city: row[8],
#             delivered_on: row[16]
#         }
#         if !existing_order
#             order_entry = {
#                 order_code:order_code,
#                 order_date:  order_date,
#                 portal:@portal,
#                 invoice_number: invoice_number,
#                 invoice_date: invoice_date,
#                 sku: sku,
#                 quantity: quantity,
#                 ship_details: ship_details,
#                 status: status
#             }
#             @orders_db.insert(order_entry)
#             transaction_entry = {
#                 order_code:order_code,
#                 order_date:  order_date,
#                 portal:@portal,
#                 invoice_number: invoice_number,
#                 invoice_date: invoice_date,
#                 sku: sku,
#                 quantity: quantity,
#                 # selling_price:row[10],
#                 # shipping_charged:row[11],
#                 # total_amount: row[15],
#                 # invoice_amount:row[17],
#                 # vat_charged:row[19],
#             }
#             # product = @input_db.find_one({sku:sku},{tax:1})
#             # transaction_entry[:actual_vat] = row[15] - (row[15].to_f / (1 + (product['tax'].to_f)/100) )
#             @transactions_db.insert(transaction_entry)
#         else
#             puts "Existing in the database #{existing_order}"
#             puts "From the CSV #{row}"
#             if existing_order['status'] != status
#                 @orders_db.update({order_code:order_code},{status:status,ship_details:ship_details})
#             end
#         end
#     end
# end






