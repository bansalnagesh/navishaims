#Script to enter the bills in the mongo system
#Usage is ruby input_bill.rb csv_filename
#The script can be ran from anywhere
#The input file needs to be of the following format bill_id, date_of_bill, supplier_id, amount, supplier_bill_no,cash_discount
# ruby input_bill.rb /Users/bansalnagesh/Dropbox/DB/Bill.csv


require 'mongo'
require 'json'
require 'csv'

if ARGV[0].nil?
  abort('Usage ruby input_bill.rb csv_filename')
end

@csv_file = ARGV[0]

DB = Mongo::Connection.new('localhost',27017,:slave_ok => true, :pool_size => 20, :pool_timeout => 5).db('navisha')

@bill_db = DB['bill']

csv_contents = CSV.read(@csv_file)
csv_contents.shift
csv_contents.each do |row|
    date_of_bill = row[1].split("/")
    date = Time.new(date_of_bill[2],date_of_bill[1],date_of_bill[0])
  bill_entry = {
      bill_id: row[0].downcase,
      date_of_bill: date,
      supplier_id: row[2],
      amount: row[3].to_f,
      supplier_bill_no: row[4],
      cash_discount: row[5].to_f,
      additional_charges: row[6].to_f
  }
  existing_entry = @bill_db.find_one({bill_id: row[0]})
  @bill_db.insert(bill_entry) if existing_entry.nil?
end